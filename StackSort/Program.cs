﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StackSort
{
    class Program
    {
        static void Main()
        {
            int filledFlasks = 12; // Количество заполненных пробирок, следовательно количество использованных цветов.
            //int filledFlasks = Enum.GetNames(typeof(Colors)).Length;
            int emptyFlasks = 3;
            int allFlasksAmount = filledFlasks + emptyFlasks;

            //Flask[] flasks = GetArrayOfFlasksFilledColors(allFlasksAmount, filledFlasks, colorsInEachFlask);

            //---------------------------------------
            /*Flask[] flasks = new Flask[filledFlasks + emptyFlasks];

            flasks[0] = new Flask();
            flasks[0].Push(Colors.Red);
            flasks[0].Push(Colors.Blue);
            flasks[0].Push(Colors.Red);
            flasks[0].Push(Colors.Blue);

            flasks[1] = new Flask();
            flasks[1].Push(Colors.Red);
            flasks[1].Push(Colors.Green);
            flasks[1].Push(Colors.Blue);
            flasks[1].Push(Colors.Green);

            flasks[2] = new Flask();
            flasks[2].Push(Colors.Blue);
            flasks[2].Push(Colors.Green);
            flasks[2].Push(Colors.Red);
            flasks[2].Push(Colors.Green);

            flasks[3] = new Flask();*/
            //---------------------------------------

            //---------------------------------------
            Flask[] flasks = new Flask[filledFlasks + emptyFlasks];

            flasks[0] = new Flask();
            flasks[0].Push(Colors.Brown);
            flasks[0].Push(Colors.Orange);
            flasks[0].Push(Colors.Grey);
            flasks[0].Push(Colors.Brown);

            flasks[1] = new Flask();
            flasks[1].Push(Colors.Purple);
            flasks[1].Push(Colors.LightBlue);
            flasks[1].Push(Colors.LightGreen);
            flasks[1].Push(Colors.Pink);

            flasks[2] = new Flask();
            flasks[2].Push(Colors.Red);
            flasks[2].Push(Colors.Orange);
            flasks[2].Push(Colors.LightGreen);
            flasks[2].Push(Colors.Grey);

            flasks[3] = new Flask();
            flasks[3].Push(Colors.Pink);
            flasks[3].Push(Colors.Brown);
            flasks[3].Push(Colors.Red);
            flasks[3].Push(Colors.Blue);

            flasks[4] = new Flask();
            flasks[4].Push(Colors.Green);
            flasks[4].Push(Colors.Blue);
            flasks[4].Push(Colors.Yellow);
            flasks[4].Push(Colors.Orange);

            flasks[5] = new Flask();
            flasks[5].Push(Colors.Blue);
            flasks[5].Push(Colors.Orange);
            flasks[5].Push(Colors.Aquamarine);
            flasks[5].Push(Colors.Yellow);

            flasks[6] = new Flask();
            flasks[6].Push(Colors.LightBlue);
            flasks[6].Push(Colors.Aquamarine);
            flasks[6].Push(Colors.Aquamarine);
            flasks[6].Push(Colors.Blue);

            flasks[7] = new Flask();
            flasks[7].Push(Colors.LightGreen);
            flasks[7].Push(Colors.Green);
            flasks[7].Push(Colors.Purple);
            flasks[7].Push(Colors.Pink);

            flasks[8] = new Flask();
            flasks[8].Push(Colors.Yellow);
            flasks[8].Push(Colors.Green);
            flasks[8].Push(Colors.Aquamarine);
            flasks[8].Push(Colors.LightGreen);

            flasks[9] = new Flask();
            flasks[9].Push(Colors.LightBlue);
            flasks[9].Push(Colors.Brown);
            flasks[9].Push(Colors.Grey);
            flasks[9].Push(Colors.Purple);

            flasks[10] = new Flask();
            flasks[10].Push(Colors.LightBlue);
            flasks[10].Push(Colors.Grey);
            flasks[10].Push(Colors.Purple);
            flasks[10].Push(Colors.Green);

            flasks[11] = new Flask();
            flasks[11].Push(Colors.Pink);
            flasks[11].Push(Colors.Red);
            flasks[11].Push(Colors.Red);
            flasks[11].Push(Colors.Yellow);

            flasks[12] = new Flask();

            flasks[13] = new Flask();

            flasks[14] = new Flask();
            //---------------------------------------


            flasks.PrintFlasks();

            Solver solver = new Solver();
            Queue<Transfusion> transfusions = solver.GetSolveNonStatic(flasks);

            //Stack<Transfusion> transfusions = Solver.GetSolve(flasks, filledFlasks);

            //while (transfusions.Count > 0)
            //{
            //    solver.MakeTransfusion(transfusions.Dequeue(), flasks);
            //}
            //Printer.PrintFlasks(flasks);

            transfusions.PrintTransfusions();

            Console.ReadLine();
        }

        
    }
}
