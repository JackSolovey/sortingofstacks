﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StackSort
{
    class StackOnList : ICloneable
    {
        private List<Colors> colors = new List<Colors>();

        public int Count => colors.Count;

        public void Push(Colors color)
        {
            colors.Add(color);
        }

        /// <summary>
        /// Добавить элемент в стек.
        /// </summary>
        /// <returns></returns>
        public Colors Pop()
        {
            if (colors.Count > 0)
            {
                var color = colors.LastOrDefault();
                colors.RemoveAt(colors.Count - 1);
                return color;
            }
            else
            {
                throw new NullReferenceException("Стек пуст");
            }
        }

        /// <summary>
        /// Показать последний добавленный элемент стека без удаления.
        /// </summary>
        /// <returns></returns>
        public Colors Peek()
        {
            if (colors.Count > 0)
            {
                return colors.LastOrDefault();
            }
            else
            {
                throw new NullReferenceException("Стек пуст");
            }
        }

        /// <summary>
        /// Возвращает true, если все элементы в стеке равны между собой.
        /// </summary>
        /// <returns></returns>
        public bool CompareAllColors()
        {
            if (colors.Count == 1)
            {
                return true;
            }
            else if (colors.Count > 1)
            {
                bool isEqual = true;
                for (int i = 0; i < colors.Count - 1; i++)
                {
                    isEqual = isEqual && (colors[i].CompareTo(colors[i + 1]) == 0);
                    if (!isEqual)
                    {
                        return false;
                    }
                }
                return isEqual;
            }
            else
            {
                throw new ArgumentOutOfRangeException("Элементов в стеке < 1");
            }
        }

        /// <summary>
        /// Получить цвет по заданному номеру.
        /// </summary>
        /// <param name="number"></param>
        /// <returns>Возвращает цвет типа Colors по заданному номеру.</returns>
        public Colors GetColorNumber(int number)
        {
            if (number < this.colors.Count)
            {
                return colors[number];
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }
        }

        public int GetAmountOfTopSameColors()
        {
            int count = 0;
            if (this.colors.Count > 0)
            {
                Colors topColor = colors[colors.Count - 1];
                for (int i = colors.Count - 1; i >= 0; i--)
                {
                    if (colors[i] == topColor)
                    {
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            return count;
        }

        public object Clone()
        {
            StackOnList result = new StackOnList();

            if (this.colors.Count > 0)
            {
                for (int i = 0; i < this.colors.Count; i++)
                {
                    result.Push(colors[i]);
                }
            }

            return result;
        }

        public override string ToString()
        {
            if (colors.Count == 0)
            {
                return "Стек пустой.";
            }
            else if (colors.Count == 1)
            {
                return $"[0]: {this.colors[0]}";
                    }
            else if (colors.Count == 2)
            {
                return $"[0]: {this.colors[0]} " +
                    $"[1]: {this.colors[1]}";
            }
            else if (colors.Count == 3)
            {
                return $"[0]: {this.colors[0]} " +
                    $"[1]: {this.colors[1]} " +
                    $"[2]: {this.colors[2]}";
            }
            else
            {
                return $"[0]: {this.colors[0]} " +
                    $"[1]: {this.colors[1]} " +
                    $"[2]: {this.colors[2]} " +
                    $"[3]: {this.colors[3]}";
            }
        }
    }
}
