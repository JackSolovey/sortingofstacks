﻿using System;

namespace StackSort
{
    public class Flask : ICloneable
    {
        private StackOnList stackOfColorsInFlask;
        public const int MAX_COLORS_IN_EACH_FLASK = 4;

        /// <summary>
        /// Возвращает количество имеющихся цветов в пробирке.
        /// </summary>
        public int Count => this.stackOfColorsInFlask.Count;

        /// <summary>
        /// Свойство. Возвращает true, если в стеке экземпляра Flask 
        /// максимально допустимое количество элементов и все элементы равны.
        /// </summary>
        public bool IsSolved { get; private set; }

        /// <summary>
        /// Свойство. Возвращает true, если в стеке экземпляра Flask 
        /// максимально допустимое количество элементов.
        /// </summary>
        public bool IsFull { get; private set; }

        /// <summary>
        /// Свойство. Возвращает true, если в стеке экземпляра Flask 
        /// нет ни одного элемента.
        /// </summary>
        public bool IsEmpty { get; private set; }

        public bool IsAllColorsSame { get; private set; }

        public int AmountOfTopSameColors { get; private set; }

        public Flask()
        {
            this.stackOfColorsInFlask = new StackOnList();
            this.IsFull = false;
            this.IsEmpty = true;
            this.IsSolved = false;
            this.IsAllColorsSame = false;
            this.AmountOfTopSameColors = 0;
        }

        public void Push(Colors color)
        {
            this.stackOfColorsInFlask.Push(color);
            this.IsEmpty = false;

            if (this.stackOfColorsInFlask.Count == MAX_COLORS_IN_EACH_FLASK)
            {
                this.IsFull = true;
                this.IsSolved = this.stackOfColorsInFlask.CompareAllColors();
            }
            else if (this.stackOfColorsInFlask.Count < MAX_COLORS_IN_EACH_FLASK)
            {
                this.IsAllColorsSame = this.stackOfColorsInFlask.CompareAllColors();
            }
            else if (this.stackOfColorsInFlask.Count > MAX_COLORS_IN_EACH_FLASK)
            {
                throw new StackOverflowException("Стек переполнен.");
            }

            this.AmountOfTopSameColors = stackOfColorsInFlask.GetAmountOfTopSameColors();
        }

        public Colors Peek()
        {
            return this.stackOfColorsInFlask.Peek();
        }

        public Colors Pop()
        {
            var result = this.stackOfColorsInFlask.Pop();
            this.IsFull = false;

            if (this.stackOfColorsInFlask.Count == 0)
            {
                this.IsEmpty = true;
                this.IsAllColorsSame = false;
            }
            else
            {
                this.IsAllColorsSame = this.stackOfColorsInFlask.CompareAllColors();
            }

            this.AmountOfTopSameColors = stackOfColorsInFlask.GetAmountOfTopSameColors();

            return result;
        }

        public object Clone()
        {
            return new Flask()
            {
                IsFull = this.IsFull,
                IsSolved = this.IsSolved,
                IsEmpty = this.IsEmpty,
                stackOfColorsInFlask = (StackOnList)this.stackOfColorsInFlask.Clone(),
                IsAllColorsSame = this.IsAllColorsSame,
                AmountOfTopSameColors = this.AmountOfTopSameColors
            };
        }

        public void Print()
        {
            for (int i = MAX_COLORS_IN_EACH_FLASK - 1; i >= 0; i--)
            {
                if (i < this.stackOfColorsInFlask.Count)
                {
                    Console.WriteLine(this.stackOfColorsInFlask.GetColorNumber(i));
                }
                else
                {
                    Console.WriteLine("Empty");
                }
            }
        }

        public override string ToString()
        {
            return $"{this.stackOfColorsInFlask.ToString()}";
        }
    }

    public enum Colors
    {
        Red,
        Green,
        LightGreen,
        Blue,
        LightBlue,
        Yellow,
        Brown,
        Orange,
        Coral,
        Aquamarine,
        Grey,
        Silver,
        Pink,
        Golden,
        Purple
    }
}
