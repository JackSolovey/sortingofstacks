﻿using System;
namespace StackSort
{
    public class Transfusion : ICloneable
    {
        /// <summary>
        /// Позиция в стеке экземпляра класса Flask из которой проведено переливание цвета Color.
        /// </summary>
        public int[] FromPositionInFlask { get; private set; }

        /// <summary>
        /// Позиция в стеке экземпляра класса Flask в которую проведено переливание цвета Color.
        /// </summary>
        public int[] IntoPositionInFlask { get; private set; }

        /// <summary>
        /// Цвет, который переливался в данной операции.
        /// </summary>
        public Colors Color { get; private set; }

        /// <summary>
        /// Позиция экземпляра класса Flask в массиве, из которого проведено переливание.
        /// </summary>
        public int FromNumberFlaskInArray { get; private set; }

        /// <summary>
        /// Позиция экземпляра класса Flask в массиве, в которую проведено переливание.
        /// </summary>
        public int IntoNumberFlaskInArray { get; private set; }

        /// <summary>
        /// Количество позиций Color, которое перелито в данном переливании.
        /// </summary>
        public int TransfusionsAmount { get; private set; }

        private Transfusion()
        {

        }

        //public Transfusion(Flask[] flasks, int fromFlaskNumber, int intoFlaskNumber)
        //{
        //    FromNumberFlaskInArray = fromFlaskNumber;
        //    IntoNumberFlaskInArray = intoFlaskNumber;
        //    FromPositionInFlask = flasks[fromFlaskNumber].Count - 1;
        //    IntoPositionInFlask = flasks[intoFlaskNumber].Count;
        //    Color = flasks[fromFlaskNumber].Peek();
        //}

        public Transfusion(Flask[] flasks, int fromFlaskNumber, int intoFlaskNumber)
        {
            this.FromNumberFlaskInArray = fromFlaskNumber;
            this.IntoNumberFlaskInArray = intoFlaskNumber;
            this.Color = flasks[fromFlaskNumber].Peek();

            // Сколько ячеек краски может вместить принимающая пробирка.
            int intoFlaskCapacity = Flask.MAX_COLORS_IN_EACH_FLASK - flasks[intoFlaskNumber].Count;

            // Сколько одинаковых ячеек краски может отдать отдающая пробирка.
            int fromFlaskOutputAmount = flasks[fromFlaskNumber].AmountOfTopSameColors;

            // Сколько будем делать переливаний.
            this.TransfusionsAmount = Math.Min(intoFlaskCapacity, fromFlaskOutputAmount);

            // Создаём массивы
            this.FromPositionInFlask = new int[this.TransfusionsAmount];
            this.IntoPositionInFlask = new int[this.TransfusionsAmount];

            for (int i = 0; i < this.TransfusionsAmount; i++)
            {
                this.FromPositionInFlask[i] = flasks[fromFlaskNumber].Count - 1 - i;
                this.IntoPositionInFlask[i] = flasks[intoFlaskNumber].Count + i;
            }
        }

        public override string ToString()
        {
            return $"Перелить {TransfusionsAmount} позиций " +
                $"краски {Color} " +
                $"из пробирки {FromNumberFlaskInArray + 1} " +
                $"в пробирку {IntoNumberFlaskInArray + 1}";
        }

        //public object Clone()
        //{
        //    throw new NotImplementedException("Не переработано клонирование массивов");
        //    return new Transfusion
        //    {
        //        FromNumberFlaskInArray = this.FromNumberFlaskInArray,
        //        IntoNumberFlaskInArray = this.IntoNumberFlaskInArray,
        //        FromPositionInFlask = this.FromPositionInFlask,
        //        IntoPositionInFlask = this.IntoPositionInFlask,
        //        Color = this.Color,
        //        TransfusionsAmount = this.TransfusionsAmount
        //    };
        //}

        public object Clone()
        {
            int[] fromPositionInFlask = new int[this.FromPositionInFlask.Length];
            int[] intoPositionInFlask = new int[this.IntoPositionInFlask.Length];
            for (int i = 0; i < fromPositionInFlask.Length; i++)
            {
                fromPositionInFlask[i] = this.FromPositionInFlask[i];
                intoPositionInFlask[i] = this.IntoPositionInFlask[i];
            }

            return new Transfusion
            {
                FromNumberFlaskInArray = this.FromNumberFlaskInArray,
                IntoNumberFlaskInArray = this.IntoNumberFlaskInArray,
                FromPositionInFlask = fromPositionInFlask,
                IntoPositionInFlask = intoPositionInFlask,
                Color = this.Color,
                TransfusionsAmount = this.TransfusionsAmount
            };
        }
    }
}
