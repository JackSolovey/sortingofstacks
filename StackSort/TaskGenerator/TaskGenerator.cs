﻿using System;
using System.Collections.Generic;

namespace StackSort
{
    static class TaskGenerator
    {
        static Stack<Colors> GetFlaskFilledAllColors(int amountOfColors, int colorsInEachFlask)
        {
            Random rnd = new Random();

            Stack<Colors> flaskFilledAllColors = new Stack<Colors>();

            int[] countOfEachColor = new int[amountOfColors];

            // Рандомно заполняем большую пробирку,
            // из которой потом будем разливать.
            while (flaskFilledAllColors.Count < amountOfColors * colorsInEachFlask)
            {
                int numberOfColor = rnd.Next(amountOfColors);
                if (countOfEachColor[numberOfColor] < colorsInEachFlask)
                {
                    countOfEachColor[numberOfColor]++;
                    flaskFilledAllColors.Push((Colors)numberOfColor);
                }
            }

            return flaskFilledAllColors;
        }

        static Flask[] GetArrayOfFlasksFilledColors(int allFlasksAmount, int filledFlasks, int colorsInEachFlask)
        {
            Stack<Colors> flaskFilledAllColors = GetFlaskFilledAllColors(filledFlasks, colorsInEachFlask);

            Flask[] flasks = new Flask[allFlasksAmount];
            // Создаём сами пробирки в массиве.
            for (int k = 0; k < allFlasksAmount; k++)
            {
                flasks[k] = new Flask();
            }

            int flasksCount = 0;
            bool isFillingHappened;
            if (flaskFilledAllColors != null)
            {
                while (flaskFilledAllColors.Count != 0)
                {
                    isFillingHappened = false;
                    if (flasks[flasksCount].Count == 0 ||
                        (!flasks[flasksCount].IsFull && flasks[flasksCount].Peek() != flaskFilledAllColors.Peek()))
                    {
                        flasks[flasksCount].Push(flaskFilledAllColors.Pop());
                        isFillingHappened = true;
                    }

                    if (flasksCount < filledFlasks - 1)
                        flasksCount++;
                    else
                    {
                        flasksCount = 0;
                        // Если прошли итерацию цикла, а заполнения не случилось
                        // то заполняем первую не заполненную пробирку принудительно.
                        if (!isFillingHappened)
                        {
                            for (int i = 0; i < filledFlasks; i++)
                            {
                                if (!flasks[i].IsFull)
                                {
                                    flasks[i].Push(flaskFilledAllColors.Pop());
                                    break;
                                }
                            }
                        }
                    }
                }
                return flasks;
            }
            else
            {
                Console.Error.WriteLine("flaskFilledAllColors == null");
                return null;
            }
        }

    }
}
