﻿using System;
using System.Collections.Generic;

namespace StackSort
{
    class Solver
    {
        private static List<Transfusion> makedTransfusions = new List<Transfusion>();

        /// <summary>
        /// Получить переливания, необходимые для решения задачи.
        /// </summary>
        /// <param name="flasks"></param>
        /// <param name="filledFlasks"></param>
        /// <returns>Возвращает стек, содержащий последовательные перелаивания для решения задачи.</returns>
        public static Stack<Transfusion> GetSolve(Flask[] flasks, int filledFlasks)
        {
            // Итераций должно быть не более
            // (flasks.Length * 4 - 1) * filledFlasks * 4
            // Это число сколько раз можно каждую ячейку перелить в каждую другую ячейку.

            int iterationCounter = (flasks.Length * 4 - 1) * filledFlasks * 4;
            Trie trie = new Trie(flasks);
            List<Transfusion> transfusionsList = new List<Transfusion>();

            bool isTaskSolve = false;
            int flasksAmount = flasks.Length;
            int stateLevelNumber = 0;
            while(!isTaskSolve) // Или всё таки счётчик меньше (flasks.Length * 4 - 1) * filledFlasks * 4
            {
                stateLevelNumber++;

                for (int from = 0; from < flasksAmount; from++)
                {
                    if (!flasks[from].IsEmpty && !flasks[from].IsSolved)
                    {
                        for (int into = 0; into < flasksAmount; into++)
                        {
                            if (!flasks[into].IsSolved && from != into && !flasks[into].IsFull)
                            {
                                // Если принимающая пробирка пустая или верхние цвета одинаковые, то можно перелить
                                if (flasks[into].IsEmpty || (flasks[from].Peek() == flasks[into].Peek()))
                                {
                                    // Переливание сейчас состоится, нужно записать в дерево ходов.
                                    Transfusion currentTransfusion = new Transfusion(flasks, from, into);

                                    // Записываем переливание в лист.
                                    transfusionsList.Add(currentTransfusion);

                                    // Само переливание.
                                    flasks[into].Push(flasks[from].Pop());

                                    // Записать новое состояние в дерево
                                    trie.Add(transfusionsList, flasks, stateLevelNumber);
                                }
                            }

                            // Если состоялось переливание и записали в дерево, то
                            // систему нужно обновить до пердыдущего состояния
                        }
                    }
                }

            }

            return null;
        }

        public Queue<Transfusion> GetSolveNonStatic(Flask[] flasks)
        {
            Queue<Transfusion> result = new Queue<Transfusion>();
            int flasksAmount = flasks.Length;

            for (int from = 0; from < flasksAmount; from++)
            {
                if (!flasks[from].IsEmpty && !flasks[from].IsSolved)
                {
                    for (int into = 0; into < flasksAmount; into++)
                    {
                        if (IsTransfusionAvailable(flasks, from, into))
                        {
                            Transfusion currentTransfusion = new Transfusion(flasks, from, into);

                            if (makedTransfusions.ContainsTransfusion(currentTransfusion))
                                continue;

                            makedTransfusions.Add(currentTransfusion);

                            // Скопировать массив.
                            Flask[] newFlasks = flasks.DeepClone();

                            // В скопированном масиве сделать переливание.
                            MakeTransfusion(currentTransfusion, newFlasks);

                            // Проверить перлитый массив на решение.
                            bool isSolved = CheckArraySolved(newFlasks);

                            // Если перелитый массив решён, то текущее переливание запушить в стек и вернуть.
                            if (isSolved)
                            {
                                result.Enqueue(currentTransfusion);
                                return result;
                            }
                            else
                            {
                                // Если перелитый массив всё ещё не решён , то 
                                // создать новый экземпляр Солвера и в него передать ссылку на перелитый массив.
                                Solver solver = new Solver();
                                Queue<Transfusion> nextStepTransfusions = solver.GetSolveNonStatic(newFlasks);

                                if (nextStepTransfusions.Count > 0)
                                {
                                    result.Enqueue(currentTransfusion);
                                    while (nextStepTransfusions.Count > 0)
                                    {
                                        result.Enqueue(nextStepTransfusions.Dequeue());
                                    }
                                    return result;
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Проверяет заполненные элементы массива на соответствие решению.
        /// </summary>
        /// <param name="flasks"></param>
        /// <returns>Возвращает true в случае, если заполненные элементы массива содержат одинаковые цвета внути себя.</returns>
        private bool CheckArraySolved(Flask[] flasks)
        {
            bool isSolved = true;
            for (int i = 0; i < flasks.Length; i++)
            {
                // Пустые пробирки пропускаем, так как они не числятся решенными.
                if (!flasks[i].IsEmpty)
                    isSolved = isSolved && flasks[i].IsSolved;
            }
            return isSolved;
        }

        /// <summary>
        /// Делает перемещение Color из одного элемента массива flasks в другой, в соответствии с transfusion.
        /// </summary>
        /// <param name="transfusion"></param>
        /// <param name="flasks"></param>
        public void MakeTransfusion(Transfusion transfusion, Flask[] flasks)
        {
            for (int i = 0; i < transfusion.TransfusionsAmount; i++)
            {
                flasks[transfusion.IntoNumberFlaskInArray].Push(flasks[transfusion.FromNumberFlaskInArray].Pop());
            }
        }

        /// <summary>
        /// Определяет возможно ли сделать переливание с переданными аргументами.
        /// </summary>
        /// <param name="flasks">Массив с экземплярами класса Flask.</param>
        /// <param name="from">Порядковый номер в массиве из которого предполагается переливание.</param>
        /// <param name="into">Порядковый номер в массиве в который предполагается переливание.</param>
        /// <returns>Возвращает true, если переливание с переданными аргументми возможно. 
        /// В других случаях возвращает false.</returns>
        private bool IsTransfusionAvailable(Flask[] flasks, int from, int into)
        {
            if (from != into) // Не пытаемся перелить в саму себя.
            {
                if (!flasks[into].IsSolved) // Принимающая пробирка не решена.
                {
                    if (!flasks[into].IsFull) // Принимающая пробирка не полная.
                    {
                        // Принимающая пробирка пустая или верхние цвета одинаковые.
                        if ((flasks[into].IsEmpty || (flasks[from].Peek() == flasks[into].Peek())))
                        {
                            // Не пытаемся перелить в пустую пробирку из пробирки в которой только одинаковые цвета
                            if (!(flasks[into].IsEmpty && flasks[from].IsAllColorsSame))
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Определяет существует ли приоритетное переливание для переданных параметров.
        /// Проиоритетным считается переливание, когда все позиции принимающего экземпляра Flask 
        /// равны верхней позиции отдающего экземпляра Flask.
        /// </summary>
        /// <param name="flasks">Массив с экземплярами класса Flask.</param>
        /// <param name="from">Порядковый номер в массиве из которого предполагается переливание.</param>
        /// <returns>Возвращает новый экземпляр Transfusion, если такое переливание найдено,
        /// в остальных случаях возвращает null.</returns>
        private Transfusion GetPriorityTransfusion(Flask[] flasks, int from)
        {
            List<Transfusion> possibleTransfusions = new List<Transfusion>();

            for (int into = 0; into < flasks.Length; into++)
            {
                if (from != into && 
                    !flasks[into].IsEmpty && 
                    (flasks[from].Peek() == flasks[into].Peek()) &&
                    flasks[into].IsAllColorsSame)
                {
                    possibleTransfusions.Add(new Transfusion(flasks, from, into));
                }
            }

            if (possibleTransfusions.Count == 1)
            {
                return possibleTransfusions[0];
            }
            else if (possibleTransfusions.Count > 1)
            {
                int numberWithMostColors = 0;
                for (int i = 1; i < possibleTransfusions.Count; i++)
                {
                    if (flasks[possibleTransfusions[i].IntoNumberFlaskInArray].AmountOfTopSameColors >
                        flasks[possibleTransfusions[numberWithMostColors].IntoNumberFlaskInArray].AmountOfTopSameColors)
                    {
                        numberWithMostColors = i;
                    }
                }
                return possibleTransfusions[numberWithMostColors];
            }
            else
            {
                return null;
            }
        }
    }
}
