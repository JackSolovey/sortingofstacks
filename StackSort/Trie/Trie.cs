﻿using System;
using System.Collections.Generic;

namespace StackSort
{
    class Trie
    {
        private List<List<FlasksInfo>> listOfAllStates = new List<List<FlasksInfo>>();

        public Trie(Flask[] flasks)
        {
            listOfAllStates = new List<List<FlasksInfo>>();
            listOfAllStates.Add(new List<FlasksInfo>());

            FlasksInfo flasksInfo = new FlasksInfo(null, flasks);
            listOfAllStates[0].Add(flasksInfo);
        }

        public void Add(List<Transfusion> curTransfusions, Flask[] curFlasks, int stateLevelNumber)
        {
            FlasksInfo curInfo = new FlasksInfo(curTransfusions, curFlasks);

            if (GetExistenceOfCurrentState(curInfo))
            {
                if (listOfAllStates.Count < stateLevelNumber)
                {
                    listOfAllStates.Add(new List<FlasksInfo>());
                }
                listOfAllStates[stateLevelNumber].Add(new FlasksInfo(curTransfusions, curFlasks));
            }
        }

        private bool GetExistenceOfCurrentState(FlasksInfo curInfo)
        {
            foreach (var listOfLevelStates in listOfAllStates)
            {
                foreach (var info in listOfLevelStates)
                {
                    if (curInfo == info)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
