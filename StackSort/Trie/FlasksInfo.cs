﻿using System;
using System.Collections.Generic;

namespace StackSort
{
    class FlasksInfo : IComparable
    {
        private List<Transfusion> transfusions; // key для Dictionary. (Переливания, которые привели к состоянию этой системы)
        // value для Dictionary - это сама нода
        private Flask[] flasks; // Состояние системы.

        public FlasksInfo(List<Transfusion> transfusions, Flask[] stateOfSystem)
        {
            // Когда создали очередную операцию переливания нужно проверить
            // не стала ли решена задача.
            this.transfusions = new List<Transfusion>();
            if (transfusions.Count != 0)
            {
                foreach (var e in transfusions)
                {
                    this.transfusions.Add((Transfusion)e.Clone());
                }
            }

            this.flasks = new Flask[stateOfSystem.Length];
            for (int i = 0; i < stateOfSystem.Length; i++)
            {
                this.flasks[i] = (Flask)stateOfSystem[i].Clone();
            }
        }

        public int CompareTo(object obj)
        {
            throw new NotImplementedException();
        }
    }
}
