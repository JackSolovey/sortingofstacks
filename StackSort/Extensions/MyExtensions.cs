﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StackSort
{
    public static class MyExtensions
    {
        /// <summary>
        /// Выводит в консоль даные класса Transfusion, содержащиеся в экземпляре Queue<Transfusion>.
        /// Queue<Transfusion> после вывода остаётся неизменной.
        /// </summary>
        /// <param name="transfusions"></param>
        public static void PrintTransfusions(this Queue<Transfusion> transfusions)
        {
            if (transfusions.Count == 0)
            {
                Console.WriteLine("Задача не решена, получено 0 ходов.");
            }
            else if (transfusions == null)
            {
                throw new ArgumentNullException();
            }
            else
            {
                for (int i = 0; i < transfusions.Count; i++)
                {
                    var transfusion = transfusions.Dequeue();
                    transfusions.Enqueue(transfusion);
                    Console.WriteLine((i + 1) + ")" + transfusion.ToString());
                }
            }
        }

        /// <summary>
        /// Выводит в консоль даные класса Flask, содержащиеся в экземпляре Flask[].
        /// </summary>
        /// <param name="flasks"></param>
        public static void PrintFlasks(this Flask[] flasks)
        {
            for (int i = 0; i < flasks.Length; i++)
            {
                Console.WriteLine("Пробирка №" + i);
                flasks[i].Print();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Создать глубокую копию массива объектов Flask.
        /// </summary>
        /// <param name="flasks"></param>
        /// <returns>Возвращает новый массив, идентичный переданному.</returns>
        public static Flask[] DeepClone(this Flask[] flasks)
        {
            Flask[] result = new Flask[flasks.Length];
            for (int i = 0; i < flasks.Length; i++)
            {
                result[i] = (Flask)flasks[i].Clone();
            }

            return result;
        }

        /// <summary>
        /// Проверяет содержится ли идентичный экземпляр класса Transfusion в
        /// данном экземпляре List<Transfusion>.
        /// (Сравнение происходит по значениям, а не по ссылкам.)
        /// </summary>
        /// <param name="transfusions"></param>
        /// <param name="transfusion"></param>
        /// <returns>Возвращает true, если идентичный экземпляр класса Transfusion
        /// содержится в данном экземпляре List<Transfusion></returns>
        public static bool ContainsTransfusion(this List<Transfusion> transfusions, Transfusion transfusion)
        {
            foreach (var e in transfusions)
            {
                if (e.FromNumberFlaskInArray == transfusion.FromNumberFlaskInArray &&
                    e.IntoNumberFlaskInArray == transfusion.IntoNumberFlaskInArray &&
                    e.FromPositionInFlask.SequenceEqual(transfusion.FromPositionInFlask) &&
                    e.IntoPositionInFlask.SequenceEqual(transfusion.IntoPositionInFlask) &&
                    e.Color == transfusion.Color &&
                    e.TransfusionsAmount == transfusion.TransfusionsAmount)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
